# QuACS

Quantum Arithmetic and Circuit Simulator

---

Designed to be kind of like a baby version of [Qiskit][1]. Implements hashmap-
and array-backed Diract state vectors and operators. Provides a library of
common gates which can be assembled into a naive circuit implementation with
compilation to the standard Hadamard/Phase/Controlled-NOT universal gate set.
Includes randomized state readout.

[1]: https://qiskit.org/

